# CH34x Driver with parity bit working properly

## Introduction
If you have just bought an USB-RS485 adapter based on the CH34x chip and you can't communicate properly with any device requiring to have enabled the parity bit then this guide is just for you.
It will help you to properly add the latest driver for the chip that fixes the parity bug.

if you are using raspbian 4.4.43-v7+, you can go directly to step 4 and use the `ch34x.ko` I have included in this repo


## 1. Get latest driver
You will need to download the latest driver from the chip manufacturer. I've added the latest driver that was available at the moment of writing this document (jan-2017) in the folder named CH341SER_LINUX

If you want to check for a new driver you should go to the following URL and download the driver yourself.
```
http://www.wch.cn/download/CH341SER_LINUX_ZIP.html
```
On the RaspberryPi unzip the downloaded driver source code into a folder named CH341SER_LINUX

## 2. Prepare Rpi Kernel Build Environment
Execute the following commands on your RaspberryPi

```sh
sudo apt-get -y install linux-headers-rpi
sudo apt-get install rpi-update
sudo rpi-update
```
Once finished, reboot your board

```sh
sudo reboot
```

Now you are running the latest kernel. Run the following commands:
```sh
sudo wget https://raw.githubusercontent.com/notro/rpi-source/master/rpi-source -O /usr/bin/rpi-source && sudo chmod +x /usr/bin/rpi-source && /usr/bin/rpi-source -q --tag-update

sudo apt-get install bc
sudo apt-get install libncurses5-dev

rpi-source
```

## 3. Build Driver Module
On the folder CH341SER_LINUX (or where you have extracted the downloaded driver) type the following commands
```sh
make
```
Once finished, if everything went well, you should have generated the file 'ch34x.ko'

## 4. Install driver
if you want to have this driver loaded automatically when your RaspberryPi boots, you should follow the following steps

```sh
sudo cp ch34x.ko /lib/modules/$(uname -r)/kernel/drivers/usb/serial

sudo depmod -a 
```

## 5. Disable old buggy driver
In my case, I was running raspbian 4.4.43-v7+ which already includes an old driver for this USB-RS485 adapter. Therefore, we need to prevent the kernel to load the old driver and make sure that our new fixed driver is loaded properly.
To accomplish this, the following steps are needed

### 1. Check if the old driver is present
```sh
ls /lib/modules/$(uname -r)/kernel/drivers/usb/serial
```
if you have a file named `ch341.ko` most probably it's the buggy driver that we need to Disable

### 3. Prevent Kernel from loading driver

```sh
sudo touch /etc/modprobe.d/disable-buggy-ch341-driver.conf
sudo vim /etc/modprobe.d/disable-buggy-ch341-driver.conf
```
Add the following lines to the created file

```
# Disable rpi stock ch341 module driver
# You should have first correctly added the latest ch34x driver module

blacklist ch341
```
save the file and reboot your board

## 6. Verification
After board boots, insert the USB-RS485 adapter and run the following commands to verify that the new driver is loaded properly

```sh
lsmod
```

you should get something similar to this

```sh
lsmod

Module                  Size  Used by
bnep                   10340  2 
hci_uart               17943  1 
btbcm                   5929  1 hci_uart
bluetooth             326105  22 bnep,btbcm,hci_uart
brcmfmac              186403  0 
brcmutil                5661  1 brcmfmac
cfg80211              428871  1 brcmfmac
rfkill                 16037  4 cfg80211,bluetooth
snd_bcm2835            20447  1 
snd_pcm                75762  1 snd_bcm2835
snd_timer              19288  1 snd_pcm
snd                    51908  5 snd_bcm2835,snd_timer,snd_pcm
bcm2835_wdt             3225  0 
bcm2835_gpiomem         3040  0 
ch34x                   6891  0 
usbserial              22115  1 ch34x
uio_pdrv_genirq         3164  0 
uio                     8000  1 uio_pdrv_genirq
i2c_dev                 5859  0 
fuse                   84037  3 
ipv6                  347556  68 
```
As you see, our new `ch34x` module was loaded

To verify that the driver is working properly, create the following python script and use a protocol analyzer to see if the parity bit is working properly.

```python
import serial

ser = serial.Serial(port='/dev/ttyUSB0', baudrate=19200, bytesize=8, parity=serial.PARITY_EVEN, stopbits=1)
ser.write(b'\tA1F3F\r')
```
Note: you should replace the device port address based on your setup

# Credits
- https://www.raspberrypi.org/forums/viewtopic.php?f=33&t=159211
- https://github.com/aperepel/raspberrypi-ch340-driver
